# MrDIY YouTube Subscriber Counter Display

In this project, I will show you how you can use the ESP8266 board Wemos D1 Mini to display any YouTube channel's subscriber count fot less than $5.
<br><br><br>
**Watch the video**: click on the image below:

[![MrDIY Audio Notifier youtube video](https://img.youtube.com/vi/0ektXT0uQLY/0.jpg)](https://www.youtube.com/watch?v=0ektXT0uQLY)


## Instructions

Please visit <a href="https://www.instructables.com/id/5-DIY-YouTube-Subscriber-Display-Using-ESP8266-No-/">my Instructables page</a> page for full instructions or watch the video above.

<p>Don't forget to check out <a href="https://www.youtube.com/channel/UCtfYdcn8F8wfRA2BXp2FPtg">my YouTube channel</a>  and follow me on <a href="https://twitter.com/MrDIYca">Twitter</a>. If you are intersted in supporting my work, please visit <a href="https://www.patreon.com/MrDIYca?fan_landing=true">my Patreon page</a>.</p>


## Thanks
Thanks to the maintainers and contributors to the ESP8266 community.




